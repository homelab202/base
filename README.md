# base
base docker images for c++ development in constrained environment.
- arch linux based
- alpine based
- ubuntu based

# dependencies
- docker
- docker-compose

# usage
build images with:
`docker-compose build`

launch all containers:
`docker-compose up --detach`

# note
To reduce recompilation of packages the anonymous volume 'artifacts-<SERVICE>' is mounted to `/stage` and could be used. Mind though, that this __impacts the reproducability__ in a constrained environment, if artifacts from an older build are present.

- docker-compose version: 2.4.1
- docker version: 20.10.14
